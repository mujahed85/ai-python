how you can use them to extract useful information from what

users say to your
Dialogflow agent.

The first video covered how
we use intents to determine

what a user wants to do.

But often when a user
expresses an intent,

they want your agent to act on
specific pieces of information

contained within
their statement.

In addition to matching
the statement to an intent,

it's often helpful to pick
out any important facts

from dates and times,
to names and numbers.

In Dialogflow, we use entities
to automatically extract

this type of information
from what the user says.

For common concepts including
dates, place names, and amounts

with units, Dialogflow can pick
out critical information using

its built-in system entities.

It's also easy to define
your own developer entities

by providing a list
of words or phrases

that fit with a given concept.

So for a bike repair
shop, you could

create one entity
for types of bike

and another for service options.

Now when we create an intent
and add example phrases,

Dialogflow will recognize which
entities might be present.

We can also annotate
any examples

that Dialogflow hasn't
recognized automatically.

When a user says something
that matches this intent,

the values for any
matching entities

will be automatically
extracted, and we

can use those values
in our backend code

to give the user what
they're asking for.

Let's see an example of
entities extracting values.

You can see a list of some
of our system and developer

entities on the right and the
user interaction on the left.

Are you open today?

Yes, we are open
until 4:00 PM today.

In this first example,
the word "today"

was automatically matched
by the system date entity.

It was resolved to a date string
that we can use in our backend.

I'd like to make an appointment
to tune up my mountain

bike next Thursday.

You're all booked
in next Thursday

for a mountain bike tuneup.

In this second example,
we matched three entities.

One of them is the system
data entity matching

"next Thursday."

The other two are
developer entities

that match a type of
service and a type of bike.

But as we all
know, conversations

are often messy and complicated.

Let's see an example that shows
how Dialogflow's AI really

shines when handling
complex conversations.

I bought a mountain bike
from your store yesterday,

but today I realized that
my road bike needs a tuneup.

Can I make an
appointment for tomorrow?

You're booked in tomorrow
for a road bike tune-up.

Anything else?

The user's question mentioned
both mountain bike and road

bike, and it specified
three dates--

yesterday, today, and tomorrow.

Despite all this
information, our agent

was able to pick out
the correct day and bike

type for the appointment.

This is possible
because entities

are identified based on
the information we provide

in our intents examples.

Dialogflow's AI can predict
where meaningful entities

are likely to appear in the text
and ignore any distractions.

Dialogflow has three
types of entities.

Let's explore how they differ.

System entities are
built into Dialogflow.

Here are just a few examples.

They cover common use cases,
such as numbers, dates

and times, amounts with
units, and geography.

For a full list, check
out the description

below for links to
our documentation.

Developer entities allow you
to define your own entity

based on a list of words, either
through the Dialogflow console,

our API, or by uploading a CSV.

Any word in the list will
be matched by the entity.

You can also provide synonyms
for the words in the list,

if there are multiple ways
of saying the same thing,

like fix versus repair.

And enabling the automated
expansion feature

allows your agent
to capture words

beyond those defined
in your entities list

by analyzing both the
position and meaning

of entities in your
intents example phrases.

You can even create
composite entities

which combine two or more
entities to describe concepts

with multiple attributes,
like blue mountain bike, which

includes both a bike
type and the color.

Finally, user entities
are special entities

that can be defined for
a specific user session.

They allow you to match
things that are transient,

like the details of a
user's previous orders

or a list of their local
store's special offers,

and they expire 10 minutes
after a conversation.

User entities are created
programmatically using our API.

We now know that intents
help us to understand

what the user wants and entities
give us the details of what

they're talking about.

But how do we turn
these building blocks

into a real conversation?

The final video in this
series will show us how,

using tools like dialogs,
contexts, and fulfillment.

Remember, take a look
at the description

for some helpful links.

Thanks for watching and have
fun building conversations.

[MUSIC PLAYING]



