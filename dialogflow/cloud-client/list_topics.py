#!/usr/bin/env python

import argparse
import json
import flask
from flask import request, jsonify

app = flask.Flask(__name__)
app.config["DEBUG"] = True

@app.route('/api/v1/resources/topics/all', methods=['GET'])
def list_topics():
    import dialogflow_v2 as dialogflow
    intents_client = dialogflow.IntentsClient()
    project_id="movie-bot-231015"
    parent = intents_client.project_agent_path(project_id)
    print("Project Id: ",project_id)
    intents = intents_client.list_intents(parent)
    print("Intents are: ",intents)
    y = json.dumps(intents)
    print("Y dataset: ",y)
# if __name__ == '__main__':
    # list_topics()
app.run()
