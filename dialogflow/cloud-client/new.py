#!/usr/bin/env python

import argparse
import json
import flask
from flask import request, jsonify
from json import JSONEncoder

app = flask.Flask(__name__)
app.config["DEBUG"] = True

#class ManageTopics(JSONEncoder):
@app.route('/api/v1/resources/topics/all', methods=['GET'])
def list_topics():
    print("list topics started...")
    import dialogflow_v2 as dialogflow
    intents_client = dialogflow.IntentsClient()
    project_id="movie-bot-231015"
    parent = intents_client.project_agent_path(project_id)
    #print("Project Id: ",project_id)
    intents = intents_client.list_intents(parent)
    topicList=[]
    for intent in intents:
        ds_icn=[]
        for icn in intent.messages: #output_contexts: #training_phrases:
               #for m1 in icn.text:
               #print("m1 is : ",m1)
               print("ICN: ",icn.text)
               ds_icn.append({"eq":icn})
        topic={
               "tagId":intent.name,
               "tagName":intent.display_name,
               "expectedq":ds_icn
        }
        #data["topic"]=intent.name
        #data["display"]=intent.display_name
        topicList.append(topic)
    return jsonify(topicList)
    #   tagid=data
    #)
    #print("Intents are: ",intents)
    #my_list=[intents]
    #print("my_list: ",my_list)
    #response = app.response_class(
    #    response=json.dumps(intents),
    #    status=200,
    #    mimetype='application/json'
    #)
    #return response
    #strData= json.dumps(my_list)
    #return jsonify({'data':strData})
    #return(str(my_list))
    #y = json.dumps(intents)
    #print("Y dataset: ",y)
    #if __name__ == '__main__':
    # list_topics()
app.run(host="0.0.0.0", port=5003)
