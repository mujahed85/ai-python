from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time
import sys
import re
# Replace below path with the absolute path
# to chromedriver in your computer
driver = webdriver.Chrome('/usr/bin/chromedriver')

#driver = webdriver.Firefox('/home/wmolewance/environments')

driver.get("https://web.whatsapp.com/")
wait = WebDriverWait(driver, 600)

target = '"whatsappbot"'

x_arg = '//span[contains(@title,' + target + ')]'
group_title = wait.until(EC.presence_of_element_located((By.XPATH, x_arg)))
group_title.click()

x_arg = '//*[@id="main"]/header/div[2]/div[1]/div'
group_details = wait.until(EC.presence_of_element_located((By.XPATH, x_arg)))
#group_details = '//*[@id="main"]/header/div[2]/div[1]/div'
group_details.click()
time.sleep(5)
for person in driver.find_elements_by_class_name('RLfQR'):
    title= person.find_element_by_xpath('//*[@id="app"]/div/div/div[2]/div[3]/span/div/span/div/div/div/div[5]/div[4]').text
    num = re.sub(r'\D', "", title)
    print(num)

driver.close()

#div/div/div[2]/div[1]/div[1]/span
#//*[@id="main"]/header/div[2]/div[1]/div/span

#//*[@id="main"]/header/div[2]/div[1]/div
#//*[@id="main"]/header/div[2]/div[1]/div/span
