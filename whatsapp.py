from time import sleep
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
import socket
from tkinter import *
import tkinter as tk           #For the GUI
import time                    #For adding delays to script
import pandas as pd            #For reading excel file
import random as rd            #For randomizing the replies
import os

#Main tkinter -- GUI
window = tk.Tk()
window.geometry('1000x650-500+200')
#window.resizable(0, 0)

window.title("CloudAge-Automate WhatsApp")

df = pd.read_csv('contacts.csv', delimiter=';')#contacts
moblie_no_list= list()
moblie_no_list = df["numbers"].tolist()

def click():
    #print ("iam a")
    message_text =messages_entry.get('1.0',END)
    #message_text =messages_entry.get()
    def element_presence(by,xpath,time):
        element_present = EC.presence_of_element_located((By.XPATH, xpath))
        WebDriverWait(driver, time).until(element_present)

    def is_connected():
        try:
        # connect to the host -- tells us if the host is actually
        # reachable
            socket.create_connection(("www.google.com", 80))
            return True
        except :
            is_connected()
#driver = webdriver.Firefox('/home/wmolewance/environments')
    driver = webdriver.Chrome('/usr/bin/chromedriver')
    driver.get("http://web.whatsapp.com")
    sleep(10) #wait time to scan the code in second

    def send_whatsapp_msg(phone_no,text):
        driver.get("https://web.whatsapp.com/send?phone={}&source=&data=#".format(phone_no))
        try:
            driver.switch_to_alert().accept()
        except Exception as e:
            pass

        try:
            element_presence(By.XPATH,'//*[@id="main"]/footer/div[1]/div[2]/div/div[2]',30)
            txt_box=driver.find_element(By.XPATH , '//*[@id="main"]/footer/div[1]/div[2]/div/div[2]')
            #text = text.replace('\n', KEYS.SHIFT+KEYS.ENTER)
            #txt_box.send_keys(text)
            #    txt_box.send_keys("\n")
            for text in text.split('\n'):
                #text= "".join(text.split("\n"))

                txt_box.send_keys(text)

                ActionChains(driver).key_down(Keys.SHIFT).key_down(Keys.ENTER).key_up(Keys.ENTER).key_up(Keys.SHIFT).perform()



            #text = text.replace('\n',"")
            #text = text.rstrip("\n")
            send=driver.find_element(By.XPATH , '//*[@id="main"]/footer/div[1]/div[3]/button/span').click()

        except Exception as e:
            print("invailid phone no :"+str(phone_no))
    for moblie_no in moblie_no_list:
        try:
            send_whatsapp_msg(moblie_no,message_text)

        except Exception as e:
            sleep(10)
            is_connected()



def close_window():
    window.destroy()

#GUI
#Labels
tk.Button(window, text="CloudAge ",width=10,font=('Times New Roman', 40,'bold'),highlightbackground='#3E4149',bg='SlateGray1',fg='deep sky blue').place(x=350,y=30)

#Entry and Label for the Contact Names
tk.Label(window, text="Enter Your Message: ",width=20,font=("bold", 20),highlightbackground='#3E4149').place(x=68,y=150)
messages_entry = tk.Text(window, height=10, width=40,font=('Verdana',16))
messages_entry.place(x=400,y=150)
# Button
tk.Button(window, text='Scan & Send', command=click, width=20,height=1,bg='green',fg='white',font=("bold",30),highlightbackground='#3E4149').place(x=400,y=450)
tk.Button(window, text='Exit', command=close_window, width=20,height=1,bg='red',fg='white',font=("bold",30),highlightbackground='#3E4149').place(x=400,y=530)

#Main Loop
window.mainloop()

#driver.close()
